def la_meilleure_ville(villes, mapChemin, n, m, g):
	modif = True
	for i in mapChemin:
		print(i)
	while modif:
		modif = False
		for i in mapChemin:
			for a in range(2*n):
				if i[a] is not None:
					vi = int(a/2)
					isC = (a%2 == 0)
					val = villes[vi]
					for b in range(2*n):
						if (mapChemin[vi][b] is not None):
							if (isC):
								old = i[b]
								if (old is None or old > val+mapChemin[vi][b]+i[a]):
									i[b] = val+mapChemin[vi][b]+i[a]
									modif = True
							else:
								old = i[b]
								if (old is None or old > mapChemin[vi][b]+i[a]):
									i[b] = mapChemin[vi][b]+i[a]
									modif = True
			
			for i in mapChemin:
				print(i)
	for i in range(n):
		c = 0
		l = 0
		for a in range(n):
			tmp = mapChemin[i][2*a]
			if (tmp is not None and a != i):
				c += tmp
				l += 1
		if (l == 0):
			print(-1)
		else:
			print(int((c+l*villes[i])/l))
		
(n, m, g) = list(map(int, input().split()))
villes = [None] * n
for i in range(0, n):
    villes[i] = int(input())
mapChemin = [[None] * (2*n) for i in range(n)]
for j in range(0, m):
	a = list(map(int, input().split()))
	b = mapChemin[a[0]-1][2*(a[1]-1)]
	if (b is not None):
		if (b > a[2]):
			b = a[2]	
	else:
		mapChemin[a[0]-1][2*(a[1]-1)] = a[2]
for k in range(0, g):
	a = list(map(int, input().split()))
	b = mapChemin[a[0]-1][(2*a[1])-1]
	if (b is not None):
		if (b > a[2]):
			mapChemin[a[0]-1][(2*a[1])-1] = a[2]
	else:
		mapChemin[a[0]-1][(2*a[1])-1] = a[2]
la_meilleure_ville(villes, mapChemin, n, m, g)  