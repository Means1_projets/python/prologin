def KnuthMorrisPratt(text, pattern, n, m):
    shifts = [1] * (len(pattern) + 1)
    shift = 1
    for pos in range(n):
        while shift <= pos and pattern[pos] != pattern[pos-shift]:
            shift += shifts[pos-shift]
        shifts[pos+1] = shift
    startPos = 0
    matchLen = 0
    for c in text:
        while matchLen == n or \
              matchLen >= 0 and pattern[matchLen] != c:
            startPos += shifts[matchLen]
            matchLen -= shifts[matchLen]
        matchLen += 1
        if matchLen == n:
            yield startPos
	
def statuettes0(motif, n, tailles, m):
	mov = [0]*(m-1)
	for i in range(m-1):
		if (tailles[i]<tailles[i+1]):
			mov[i] = 1
	motif2 = [None]*n
	for i in range(n):
		motif2[motif[i]-1] = i+1
	pattern = [0]*(n-1)
	for i in range(n-1):
		if (motif2[i]<motif2[i+1]):
			pattern[i] = 1
	a = list(KnuthMorrisPratt(mov, pattern, n-1, m-1))
	c = [False]*(len(a)-1)
	for b in range(len(a)-1):
		if (a[b+1] - a[b] < n - 1):
			c[b] = True
	print(a)
	shifts = [1] * (len(pattern) + 1)
	shift = 1
	for pos in range(n-1):
		while shift <= pos and pattern[pos] != pattern[pos-shift]:
			shift += shifts[pos-shift]
		shifts[pos+1] = shift
	print(c)
	print(shifts)
		
(n, m) = list(map(int, input().split()))
motif = list(map(int, input().split()))
tailles = list(map(int, input().split()))
statuettes0(motif, n, tailles, m) 
