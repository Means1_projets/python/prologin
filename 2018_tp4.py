import sys
sys.setrecursionlimit(150000)
def get(l, id):
	if (len(l) > 0):
		if (type(l[0]) != int):
			a = []
			for c in l:
				if (c[0] != id):
					l2 = get(c, id)
					if (len(l2) > 0):
						a.append(l2)
			return a
		elif (l[0] != id):
			return [l[0], l[1], l[2], get(l[3], id)]
		else:
			return []
	else:
		return []
def setMinScore(tmp, path, id, scoreVille, temps, first):
	if (len(path) > 0):
		if (type(path[0]) != int):
			for c in path:
				setMinScore(tmp, c, id, scoreVille, temps, first)
		elif (path[0] != id):
			value = tmp[path[0]]
			if (first):
				if (value is None or value > path[1]+scoreVille):
					tmp[path[0]] = path[1]+scoreVille
				setMinScore(tmp, path[3], id, scoreVille, path[1]+scoreVille, False)
			else:
				if (value is None or value > path[1]+temps):
					tmp[path[0]] = path[1]+temps
				setMinScore(tmp, path[3], id, scoreVille, path[1]+temps, False)
def la_meilleure_ville(villes, n, m, g):
		for a in range(n-1):
			for v in villes:
				l = []
				id = v[0]
				for b in v[2]:
					tmp = [b[0], b[1], True, []]
					ville = villes[b[0]]
					for c in get(ville[4], id):
						if (len(c) > 0):
							c[1] += ville[1]
							tmp[3].append(c)
					l.append(tmp)
				for g in v[3]:
					tmp = [g[0], g[1], False, []]
					for c in get(villes[g[0]][4], id):
						if (len(c) > 0):
							tmp[3].append(c)
					l.append(tmp)
				v[4] = l
		for v in villes:
			tmp = [None] * n
			setMinScore(tmp, v[4], v[0], v[1], 0, True)
			l = 0
			score = 0
			for i in tmp:
				if (i is not None):
					l += 1
					score += i
			if (l == 0):
				print(-1)
			else:
				score = score / l
				print(int(score))
(n, m, g) = list(map(int, input().split()))
villes = [None] * n
for i in range(0, n):
    villes[i] = [i, int(input()), [], [], []]
for j in range(0, m):
	a = list(map(int, input().split()))
	b = a[-2:]
	b[0] = b[0]-1
	villes[a[0]-1][2].append(b)
for k in range(0, g):
	a = list(map(int, input().split()))
	b = a[-2:]
	b[0] = b[0]-1
	villes[a[0]-1][3].append(b)
la_meilleure_ville(villes, n, m, g)  
